from PIL import Image
from io import BytesIO
import matplotlib.image as matimg
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.optim as optim
import requests
from torchvision import transforms, models

def get_device():
    return torch.device("cuda" if torch.cuda.is_available() else "cpu")

def load_vgg19_model_and_disable_gradient():
    vgg = models.vgg19(pretrained=True).features
    for param in vgg.parameters():
        param.requires_grad_(False)

    device = get_device()
    vgg.to(device)

    return vgg

def load_image(image_path, max_size = 400, shape = None):
    if "http" in image_path:
        response = requests.get(image_path)
        image = Image.open(BytesIO(response.content)).convert('RGB')
    else:
        image = Image.open(image_path).convert('RGB')

    if max(image.size) > max_size:
        size = max_size
    else:
        size = max(image.size)

    if shape is not None:
        size = shape

    in_transforms = transforms.Compose([
        transforms.Resize(size),
        transforms.ToTensor(),
        transforms.Normalize(
            (0.485, 0.456, 0.406),
            (0.229, 0.224, 0.225)
        )
    ])

    image = in_transforms(image)[:3,:,:].unsqueeze(0)

    return image


def image_convert(tensor):
    image = tensor.to("cpu").clone().detach()
    image = image.numpy().squeeze()
    image = image.transpose(1,2,0)
    image = image * np.array((0.229, 0.224, 0.225)) + np.array((0.485, 0.456, 0.406))
    image = image.clip(0, 1)

    return image

def get_features(image, model, layers = None):
    if layers is None:
        layers = {'0': 'conv1_1',
            '5': 'conv2_1', 
            '10': 'conv3_1', 
            '19': 'conv4_1',
            '21': 'conv4_2',  ## content representation
            '28': 'conv5_1'}
    
    features = {}
    x = image
    for name, layer in model._modules.items():
        x = layer(x)
        if name in layers:
            features[layers[name]] = x
    
    return features
    
def gram_matrix(tensor):
    batch, depth, height, width = tensor.size()
    tensor = tensor.view(batch* depth, -1)
    gram = torch.mm(tensor, tensor.t())

    return gram

def main():
    vgg = load_vgg19_model_and_disable_gradient()
    device = get_device()
    content = load_image('octopus.jpg').to(get_device())
    style = load_image('hockney.jpg', shape = content.shape[-2:]).to(get_device())

    matimg.imsave('img1.png', image_convert(content))
    matimg.imsave('img2.png', image_convert(style))

    content_features = get_features(content, vgg)
    style_features = get_features(style, vgg)

    style_grams = {layer: gram_matrix(style_features[layer]) for layer in style_features}
    target = content.clone().requires_grad_(True).to(device)

    style_weights = {'conv1_1': 1.,
                 'conv2_1': 0.8,
                 'conv3_1': 0.5,
                 'conv4_1': 0.3,
                 'conv5_1': 0.1}
    content_weight = 1  # alpha
    style_weight = 1e6  # beta

    optimizer = optim.Adam([target], lr=0.003)

    debug_info_each=400
    steps = 10000

    for ii in range(1, steps+1):
        target_features = get_features(target, vgg)
        content_loss = torch.mean((target_features['conv4_2'] - content_features['conv4_2'])**2)
        style_loss = 0
        for layer in style_weights:
            target_feature = target_features[layer]
            batch, depth, height, width = target_feature.shape

            target_gram = gram_matrix(target_feature)
            style_gram = style_grams[layer]
            layer_style_loss = style_weights[layer] * torch.mean((target_gram - style_gram)**2)
            style_loss += layer_style_loss / (depth * height * width)
        
        total_loss = content_weight * content_loss + style_weight * style_loss

        optimizer.zero_grad()
        total_loss.backward()
        optimizer.step()

        if ii % debug_info_each == 0:
            print('Total loss: ', total_loss.item())
            matimg.imsave('target_' + str(ii) + '.jpg', image_convert(target))

    matimg.imsave('endimage.jpg', image_convert(target))

if __name__ == '__main__':
    main()
